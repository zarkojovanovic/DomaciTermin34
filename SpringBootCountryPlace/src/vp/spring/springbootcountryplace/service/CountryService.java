package vp.spring.springbootcountryplace.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.spring.springbootcountryplace.data.CountryRepository;
import vp.spring.springbootcountryplace.model.Country;

@Component
public class CountryService {
	@Autowired
	CountryRepository countryRepository;
	
	public List<Country> findAll() {
		return countryRepository.findAll();
	}
	
	public Country findOne(int id) {
		return countryRepository.findOne(id);
	}
	
	public void save (Country country) {
		countryRepository.save(country);
	}
	
	public void remove(int id) {
		countryRepository.delete(id);
	}
	
	public List<Country> findByName(String name) {
		return countryRepository.findByNameContains(name);
	}
	
	public List<Country> findByPopulationGreaterThan(int population) {
		return countryRepository.findByPopulationGreaterThan(population);
	}
	
	public List<Country> finByNameOrPopulation(String name, int population) {
		return countryRepository.findByNameContainsOrPopulationGreaterThan(name, population);
	}
	
	public int classifyCountry(int countryId) {
		Country country = countryRepository.findOne(countryId);
		if (country.getPopulation() > 10000000) {
			return 1;
		} else if (country.getPopulation() > 1000000) {
			return 2;
		} else {
			return 3;
		}
	}
}
