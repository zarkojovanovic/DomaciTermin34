package vp.spring.springbootcountryplace.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.spring.springbootcountryplace.data.PlaceRepository;
import vp.spring.springbootcountryplace.model.Place;

@Component
public class PlaceService {
	@Autowired
	PlaceRepository placeRepository;
	
	public void populate(){
		 placeRepository.populate();
	}
	public List<Place> findAll() {
		return placeRepository.findAll();
	}
	
	public Place findOne(int id) {
		return placeRepository.findOne(id);
	}
	
	public void save(Place place) {
		placeRepository.save(place);
	}
	
	public void remove(int id) {
		placeRepository.delete(id);
		
	}
	
	public Place findByZipCode(int zipCode) {
		return placeRepository.findByZipCode(zipCode);
	}
	
	public List<Place> findByName(String name) {
		return placeRepository.findByNameContains(name);
	}
	
	public List<Place> findByCountry(int countryId) {
		return placeRepository.findByCountry(countryId);
	}
}
