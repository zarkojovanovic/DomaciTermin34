package vp.spring.springbootcountryplace.model;

public class Place {
	
	private int id;
	private String name;
	private int zipCode;
	private Country country;
	
	public Place() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Place(int id, String name, int zipCode, Country country) {
		super();
		this.id = id;
		this.name = name;
		this.zipCode = zipCode;
		this.country = country;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getZipCode() {
		return zipCode;
	}

	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "Place [id=" + id + ", name=" + name + ", zipCode=" + zipCode + ", country=" + country + "]";
	}

	@Override
	public int hashCode() {
		return id;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Place other = (Place) obj;
		return id == other.id;
	}
	
	

}
