package vp.spring.springbootcountryplace.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;

import vp.spring.springbootcountryplace.model.Country;

@Component("countryRepository")
public class CountryRepository {
	private List<Country> countries = new ArrayList<Country>();
	
	public List<Country> getCountries() {
		return countries;
	}
	
	public CountryRepository() {
		countries.add(new Country(1, "Serbia", 7098000));
		countries.add(new Country(2, "France", 66810000));
		countries.add(new Country(3, "Italy", 60800000));
	}
	
	public List<Country> findAll() {
		return countries;
	}
	
	public Country findOne(int id) {
		for (Country country: countries) {
			if (country.getId() == id) {
				return country;
			}
		}
		return null;
	}
	
	public void save(Country country) {
		Country existingCountry = findOne(country.getId());
		if (existingCountry == null) {
			countries.add(country);
		} else {
			existingCountry.setName(country.getName());
			existingCountry.setPopulation(country.getPopulation());
		}
	}
	
	public void delete(int id) {
		Iterator<Country> it = countries.iterator();
		while (it.hasNext()) {
			if (it.next().getId() == id) {
				it.remove();
				return;
			}
		}
	}
	
	public List<Country> findByPopulationGreaterThan(int population) {
		List<Country> retVal = new ArrayList<Country>();
		for (Country country: countries) {
			if (country.getPopulation() >= population ) {
				retVal.add(country);
			}
		}
		return retVal;
	}
	
	public List<Country> findByNameContains(String name) {
		List<Country> retVal = new ArrayList<Country>();
		for (Country country: countries) {
			if (country.getName().contains(name)) {
				retVal.add(country);
			}
		}
		return retVal;
	}
	
	public List<Country> findByNameContainsOrPopulationGreaterThan(String name, int population) {
		List<Country> retVal = new ArrayList<Country>();
		for (Country country: countries) {
			if(country.getName().contains(name) || country.getPopulation() > population) {
				retVal.add(country);
			}
		}
		return retVal;
	}
	
}
