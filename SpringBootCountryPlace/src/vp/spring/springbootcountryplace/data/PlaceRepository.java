package vp.spring.springbootcountryplace.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.spring.springbootcountryplace.model.Place;

@Component("placeRepository")
public class PlaceRepository {
	@Autowired
	CountryRepository countryRepository;
	
	private List<Place> places = new ArrayList<Place>();
	
	public List<Place> getPlaces() {
		return places;
	}

	
	public void populate() {
		places.add(new Place(1, "NoviSad", 21000,countryRepository.findOne(1)));
		places.add(new Place(2, "Belgrade", 11000, countryRepository.findOne(1)));
		places.add(new Place(3, "Paris", 75000, countryRepository.findOne(2)));
		places.add(new Place(4, "Rome", 45000, countryRepository.findOne(3)));
	}
	
	public PlaceRepository() {
		super();

		
	}




	public List<Place> findAll() {
		return places;
	}
	
	public Place findOne(int id) {
		for (Place place: places) {
			if (place.getId() == id) {
				return place;
			}
		}
		return null;
	}
	
	public void save(Place place) {
		Place existingPlace = findOne(place.getId());
		if (existingPlace == null) {
			places.add(place);
		} else {
			existingPlace.setName(place.getName());
			existingPlace.setZipCode(place.getZipCode());
			existingPlace.setCountry(place.getCountry());
		}
	}
	
	public void delete(int id) {
		Iterator<Place> it = places.iterator();
		while(it.hasNext()) {
			if (it.next().getId() == id) {
				it.remove();
				return;
			}
		}
	}
	
	public Place findByZipCode(int zipCode) {
		for (Place place: places) {
			if(place.getZipCode() == zipCode) {
				return place;
			}
		}
		return null;
	}
	
	public List<Place> findByNameContains(String name) {
		List<Place> retVal = new ArrayList<Place>();
		for (Place place: places) {
			if (place.getName().contains(name)) {
				retVal.add(place);
			}
		}
		return retVal;
		
	}
	
	public List<Place> findByCountry(int countryId) {
		List<Place> retVal = new ArrayList<Place>();
		for (Place place: places) {
			if (place.getCountry().getId() == countryId) {
				retVal.add(place);
			}
		}
		return retVal;
	}
}
