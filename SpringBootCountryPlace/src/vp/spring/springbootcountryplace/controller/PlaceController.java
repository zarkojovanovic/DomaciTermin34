package vp.spring.springbootcountryplace.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.springbootcountryplace.model.Country;
import vp.spring.springbootcountryplace.model.Place;
import vp.spring.springbootcountryplace.service.CountryService;
import vp.spring.springbootcountryplace.service.PlaceService;

@RestController
public class PlaceController {
	@Autowired
	PlaceService placeService;
	
	@Autowired
	CountryService countryService;
		
	
	
	@RequestMapping(value = "/pages/places", method = RequestMethod.GET)
	public String getPlacesHtml() {
		placeService.populate();
		List<Place> places = placeService.findAll();
		
		StringBuffer sb = new StringBuffer();
		sb.append("<table>");
		sb.append("<tr>");
		sb.append("<td>");
		sb.append("Naziv");
		sb.append("</td>");
		sb.append("<td>");
		sb.append("Zip kod");
		sb.append("</td>");
		sb.append("<td>");
		sb.append("Drzava");
		sb.append("</td>");
		sb.append("</tr>");
		
		for (Place p: places) {
			sb.append("<tr>");
			sb.append("<td>");
			sb.append(p.getName());
			sb.append("</td>");
			sb.append("<td>");
			sb.append(p.getZipCode());
			sb.append("</td>");
			sb.append("<td>");
			sb.append(p.getCountry().getName());
			sb.append("</td");
			sb.append("</tr>");
		}
		return sb.toString();
	}
	@RequestMapping(value = "pages/places/zipCode/{zipCode}", method = RequestMethod.GET) 
	public String getPlaceByZipCode(@PathVariable int zipCode) {
		placeService.populate();
		Place place = placeService.findByZipCode(zipCode);
		
		StringBuffer sb  = new StringBuffer();
		sb.append("Trazeni grad: ");
		sb.append(place.getName());
		return sb.toString();
		
	}
	
	@RequestMapping(value = "pages/places/country/{countryId}", method = RequestMethod.GET)
	public String getPlacesByCountryId(@PathVariable int countryId) {
		placeService.populate();
		List<Place> places = placeService.findByCountry(countryId);
		Country country = countryService.findOne(countryId);
		StringBuffer sb = new StringBuffer();
		sb.append("Gradovi koji se nalaze u drzavi:");
		sb.append(country.getName());
		sb.append("<table>");
		for (Place p: places) {
			sb.append("<tr>");
			sb.append("<td>");
			sb.append(p.getName());
			sb.append("</td>");
			sb.append("<td>");
			sb.append(p.getZipCode());
			sb.append("</td>");
			sb.append("</tr>");
	    }
		return sb.toString();
	}
}
