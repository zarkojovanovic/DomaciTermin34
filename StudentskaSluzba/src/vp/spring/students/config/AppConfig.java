package vp.spring.students.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("vp.spring.students") 
public class AppConfig {

}
