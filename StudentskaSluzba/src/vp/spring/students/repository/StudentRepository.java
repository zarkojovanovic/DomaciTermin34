package vp.spring.students.repository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;

import vp.spring.students.model.Student;

@Component
public class StudentRepository {
	private List<Student> students;
	
	public StudentRepository() throws IOException {
		BufferedReader inputStream = new BufferedReader(new FileReader("data/students.txt"));
		students = new ArrayList<Student>();
		String line;
		while ((line = inputStream.readLine()) != null) {
			String [] data = line.split(",");
			int id = Integer.valueOf(data[0]);
			String firstName = data[1];
			String lastName = data[2];
			String cardNumber = data[3];
			
			Student student = new Student(id, firstName, lastName, cardNumber);
			
			students.add(student);
		}
		inputStream.close();
	}
	
	public List<Student> findAll() {
		return students;
	}
	
	public Student findOne(int id) {
		for (Student student: students) {
			if (student.getId() == id) {
				return student;
			}
		}
		return null;
	}
	
	public Student findByCardNumber(String cardNumber) {
		for (Student student: students) {
			if (student.getCardNumber().equals(cardNumber)) {
				return student;
			}
		}
		return null;
	}
	
	public void save(Student student) {
		Student existingStudent = findOne(student.getId());
		if (existingStudent == null) {
			students.add(student);
		} else {
			existingStudent.setFirstName(student.getFirstName());
			existingStudent.setLastName(existingStudent.getLastName());
			existingStudent.setCardNumber(student.getCardNumber());
		}
	}
	
	public void delete(int id) {
		Iterator<Student> it = students.iterator();
		while (it.hasNext()) {
			if (it.next().getId() == id) {
				it.remove();
				return;
			}
		}
	
	}
	
	public void saveToFile() throws IOException {
		PrintWriter stream = new PrintWriter(new FileWriter("data/students.txt"));
		
		for (Student student: students) {
			stream.println(student.getId() + "," + student.getFirstName() + "," +student.getLastName()+","
					+ student.getCardNumber());
		}
		stream.close();
	}
	
}
