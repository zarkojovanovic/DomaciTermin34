package vp.spring.students.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.students.model.Student;
import vp.spring.students.service.StudentService;

@RestController
public class StudentController {
	@Autowired
	StudentService studentService;
	
	@RequestMapping(value="/student/test") 
	public String test() {
		return "Student tested";
	}
	
	@RequestMapping(value="/student/all", method = RequestMethod.GET)
	public String getAllStudents() {
		List<Student> students = studentService.findAll();
		
		StringBuffer sb = new StringBuffer();
		sb.append("<Table>");
		sb.append("<tr>");
		sb.append("<th>");
		sb.append("Ime");
		sb.append("</th>");
		sb.append("<th>");
		sb.append("Prezime");
		sb.append("</th>");
		sb.append("<th>");
		sb.append("Broj indeksa");
		sb.append("</th>");
		sb.append("</tr>");
		
		for (Student s: students) {
			sb.append("<tr>");
			sb.append("<td>");
			sb.append(s.getFirstName());
			sb.append("</td>");
			sb.append("<td>");
			sb.append(s.getLastName());
			sb.append("</td>");
			sb.append("<td>");
			sb.append(s.getCardNumber());
			sb.append("</td>");
			sb.append("</tr>");
		}
		
		return sb.toString();
			
		
	}
	
	@RequestMapping(value="/student/find/{cardNumber}", method = RequestMethod.GET) 
	public String getStudentByCardNumberHtml(@PathVariable String cardNumber) {	
		Student student = studentService.findByCardNumber(cardNumber);
		StringBuffer sb = new StringBuffer();
		sb.append("<i>");
		sb.append(student.getFirstName());
		sb.append(" ");
		sb.append(student.getLastName());
		sb.append(" ");
		sb.append(student.getCardNumber());
		sb.append("</i>");
		
		return sb.toString();
    }
}
