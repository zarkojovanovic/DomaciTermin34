package vp.spring.students.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.students.model.Course;
import vp.spring.students.model.Exam;
import vp.spring.students.model.Student;
import vp.spring.students.service.CourseService;
import vp.spring.students.service.ExamService;
import vp.spring.students.service.StudentService;

@RestController
public class ExamController {
	@Autowired
	ExamService examService;
	@Autowired
	StudentService studentService;
	@Autowired
	CourseService courseService;
	@Autowired
	StudentController studentController;
	@Autowired
	CourseController courseController;
	
	@RequestMapping(value="/exam/test") 
	public String test() {
			return "Exam tested";
		
	}
	
	@RequestMapping(value="/exam/all", method = RequestMethod.GET)
	public String getAllExams() {
		List<Exam> exams = examService.findAll();
		
		StringBuffer sb = new StringBuffer();
		sb.append("<Table>");
		sb.append("<tr>");
		sb.append("<th>");
		sb.append("Student");
		sb.append("</th>");
		sb.append("<th>");
		sb.append("Predmet");
		sb.append("</th>");
		sb.append("<th>");
		sb.append("Ocena");
		sb.append("</th>");
		sb.append("</tr>");
		
		for (Exam e: exams) {
			sb.append("<tr>");
			sb.append("<td>");
			sb.append(studentController.getStudentByCardNumberHtml(e.getStudent().getCardNumber()));
			sb.append("</td>");
			sb.append("<td>");
			sb.append(courseController.getCourseById(e.getCourse().getId()));
			sb.append("</td>");
			sb.append("<td>");
			sb.append(e.getGrade());
			sb.append("</td>");
			sb.append("</tr>");
		}
		
		return sb.toString();
	}
	
	@RequestMapping(value="exam/student/search", method= RequestMethod.GET) 
	public String getExamsByStudent(@RequestParam int studentId ) {
		List<Exam> exams = examService.findByStudentId(studentId);
		Student student = studentService.findOne(studentId);
		String delimiter = "  ";
		StringBuffer sb = new StringBuffer();
		sb.append("Polozeni ispiti  studenta: ");
		sb.append("<br />");
		sb.append(student.getFirstName());
		sb.append(delimiter);
		sb.append(student.getLastName());
		sb.append(delimiter);
		sb.append(student.getCardNumber());
		sb.append("<br />");
		
		for (Exam e: exams) {
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append(e.getCourse().getName());
			sb.append("</td>");
			sb.append("<td>");
			sb.append(e.getGrade());
			sb.append("</td>");
			sb.append("</tr>");
			
		}
		return sb.toString();
	}
	
	@RequestMapping(value="exam/course/search", method= RequestMethod.GET)
	public String getExamsByCourse(@RequestParam int courseId) {
		List<Exam> exams = examService.findByCourseId(courseId);
		Course course = courseService.findOne(courseId);
		
		String delimiter = "  ";
		StringBuffer sb = new StringBuffer();
		sb.append("Polozeni ispiti iz predmeta: ");
		sb.append("<br />");
		sb.append(course.getName());
		sb.append(delimiter);
		sb.append(course.getEspb());
		sb.append("<br /");
		
		for (Exam e: exams) {
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append(studentController.getStudentByCardNumberHtml(e.getStudent().getCardNumber()));
			sb.append("</td>");
			sb.append("<td>");
			sb.append(e.getGrade());
			sb.append("</td>");
			sb.append("</tr>");
		}
		return sb.toString();
	}
	
}
