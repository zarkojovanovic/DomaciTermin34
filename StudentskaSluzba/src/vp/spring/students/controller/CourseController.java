package vp.spring.students.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.students.model.Course;
import vp.spring.students.service.CourseService;

@RestController
public class CourseController {
	@Autowired
	CourseService courseService;
	
	
	@RequestMapping(value="/course/test")
	public String test() {
		return "Course tested";
	}
	
	@RequestMapping(value="/course/all", method = RequestMethod.GET) 
	public String getAllCourses() {
		List<Course> courses = courseService.findAll();
		
		StringBuffer sb = new StringBuffer();
		sb.append("<Table>");
		sb.append("<tr>");
		sb.append("<th>");
		sb.append("Naziv");
		sb.append("</th>");
		sb.append("<th>");
		sb.append("ESPB");
		sb.append("</th>");
		sb.append("</tr>");
		
		for (Course c: courses) {
			sb.append("<tr>");
			sb.append("<td>");
			sb.append(c.getName());
			sb.append("</td>");
			sb.append("<td>");
			sb.append(c.getEspb());
			sb.append("</td>");
			sb.append("</tr>");
		}
		
		return sb.toString();
	}
	
	@RequestMapping(value="/course/find/{id}", method = RequestMethod.GET) 
	public String getCourseById(@PathVariable int id) {
		Course course = courseService.findOne(id);
		
		StringBuffer sb = new StringBuffer();
		sb.append("<i>");
		sb.append(course.getName());
		sb.append(" ");
		sb.append(course.getEspb());
		sb.append(" ");
		sb.append("</i>");
		return sb.toString();
		
	}
	
}
