package vp.spring.students.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.spring.students.model.Student;
import vp.spring.students.repository.StudentRepository;

@Component
public class StudentService {
	@Autowired
	StudentRepository studentRepository;
	
	@Autowired
	ExamService examService;
	
	public List<Student> findAll() {
		return studentRepository.findAll();
	}
	
	public Student findOne(int id) {
		return studentRepository.findOne(id);
	}
	
	public Student findByCardNumber(String cardNumber) {
		return studentRepository.findByCardNumber(cardNumber);
	}
	
	public void save(Student student) {
		studentRepository.save(student);
	}
	
	public void remove(int id) {
		if (examService.findByStudentId(id).isEmpty()) {
			studentRepository.delete(id);
		}
	
	}
	public void persistStudent() throws IOException {
		studentRepository.saveToFile();
	}
}
